package com.pv.employeeservice.dto;

import javax.validation.constraints.NotEmpty;

public class FICenterDTO {
	private Integer id;
	@NotEmpty
	private String name;
	private FICenterDTO parentCenter;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public FICenterDTO getParentCenter() {
		return parentCenter;
	}
	public void setParentCenter(FICenterDTO parentCenter) {
		this.parentCenter = parentCenter;
	}

}
