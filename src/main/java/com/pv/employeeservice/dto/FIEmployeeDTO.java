package com.pv.employeeservice.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class FIEmployeeDTO {
	
	private Integer id;
	
	@NotEmpty
	private String foreName;
	
	@NotEmpty
	private String sureName;
	
	@NotNull
	private Date dateOfBirth;
	
	@NotEmpty
	private String email;
	
	private Double salary;	
	
	@NotEmpty
	private FIDepartmentDTO department;
	
	@NotEmpty
	private List<FIAddressDTO> addressList;
	
	@NotNull
	private FIUserDTO user;

	public FIEmployeeDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getForeName() {
		return foreName;
	}

	public void setForeName(String foreName) {
		this.foreName = foreName;
	}

	public String getSureName() {
		return sureName;
	}

	public void setSureName(String sureName) {
		this.sureName = sureName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public FIDepartmentDTO getDepartment() {
		return department;
	}

	public void setDepartment(FIDepartmentDTO department) {
		this.department = department;
	}

	public List<FIAddressDTO> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<FIAddressDTO> addressList) {
		this.addressList = addressList;
	}

	public FIUserDTO getUser() {
		return user;
	}

	public void setUser(FIUserDTO user) {
		this.user = user;
	}
	
 
}
