package com.pv.employeeservice.dto;

public class FIRoleDTO {
	private Integer id;
	private String role;
	
	public FIRoleDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
}
