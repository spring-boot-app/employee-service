package com.pv.employeeservice.dto;

import java.util.Set;

public class FIUserDTO {
	private String userName;
	private String password;
	private Set<FIRoleDTO> roles;
	
	public FIUserDTO() {
		//TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<FIRoleDTO> getRoles() {
		return roles;
	}

	public void setRoles(Set<FIRoleDTO> roles) {
		this.roles = roles;
	}
	
	
	
}
