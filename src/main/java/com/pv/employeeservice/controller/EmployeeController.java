package com.pv.employeeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pv.employeeservice.constants.EmployeeEnum;
import com.pv.employeeservice.dto.FIEmployeeDTO;
import com.pv.employeeservice.model.FIEmployee;
import com.pv.employeeservice.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;
		
	@PostMapping("/registerEmployee")
	public EmployeeEnum registerEmployee(@RequestBody FIEmployeeDTO employee){
		employeeService.registerUser(employee);
		return EmployeeEnum.SUCCESS;
	}
	
	@GetMapping("/findall")
	public List<FIEmployee> fetchAllEmployee(){
		return employeeService.fetchAllEmployee();
	}
	
}
