package com.pv.employeeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pv.employeeservice.dto.FIDepartmentDTO;
import com.pv.employeeservice.service.DepartmentService;

@RestController
@RequestMapping(value="/employee")
public class DepartmentController {
	
	@Autowired
	private DepartmentService departmentService;
	
	@PostMapping(value ="/createDepartment")
	public FIDepartmentDTO createDepartment(@RequestBody FIDepartmentDTO department) {
		return departmentService.createDepartment(department);
	}
	
	@GetMapping(value="/getAllDepartment")
	public List<FIDepartmentDTO> getAllDepartment(){
		return departmentService.getDepartments();
	}

}
