package com.pv.employeeservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pv.employeeservice.constants.EmployeeEnum;
import com.pv.employeeservice.dto.FIUserDTO;
import com.pv.employeeservice.service.FIUserService;


@RestController
@RequestMapping(value="user")
public class UserController {

	@Autowired
	private FIUserService userService;
	
	@PostMapping(value="/createUser")
	public String createUser(@RequestBody FIUserDTO userDTO) {
		userService.createUser(userDTO);
		return EmployeeEnum.SUCCESS.toString();
	}
	
	@GetMapping(value="/findAll")
	public String getAll() {
		return "success";
	}
}
