package com.pv.employeeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pv.employeeservice.dto.FICenterDTO;
import com.pv.employeeservice.service.CenterService;

@RestController
@RequestMapping("/employee")
public class CenterController {
	
	@Autowired
	private CenterService centerService;
	
	@PostMapping(value="/createCenter")
	public FICenterDTO createCenter(@RequestBody FICenterDTO center) {
		return centerService.createCenter(center);
	}
	
	@PutMapping(value="/updateCenter")
	public FICenterDTO updateCenter(@RequestBody FICenterDTO center) {
		return centerService.updateCenter(center);
	}
	
	@GetMapping(value="/getCenters")
	public List<FICenterDTO> getCenters(){
		return centerService.getAllCenter();
	}

}
