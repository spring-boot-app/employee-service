package com.pv.employeeservice.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name ="pv_entity_employee")
public class FIEmployee implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name="fore_name")
	private String foreName;
	
	@Column(name="sure_name")
	private String sureName;
	
	@Column(name="date_of_birth")
	private Date dateOfBirth;
	
	@Column(name="email")
	private String email;
	
	@Column(name="salary")
	private Double salary;	
	
	@ManyToOne
	@JoinColumn(name="department_id")
	private FIDepartment department;
	
	@OneToMany(targetEntity = FIAddress.class,mappedBy="employee",cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private List<FIAddress> addressList;
	
   @OneToOne(mappedBy = "employee", fetch = FetchType.LAZY, cascade =   CascadeType.ALL) 
   private FIUser user;
	 
	
	public FIEmployee() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getForeName() {
		return foreName;
	}

	public void setForeName(String foreName) {
		this.foreName = foreName;
	}

	public String getSureName() {
		return sureName;
	}

	public void setSureName(String sureName) {
		this.sureName = sureName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public FIDepartment getDepartment() {
		return department;
	}

	public void setDepartment(FIDepartment department) {
		this.department = department;
	}

	public List<FIAddress> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<FIAddress> addressList) {
		this.addressList = addressList;
	}

	public FIUser getUser() {
		return user;
	}

	public void setUser(FIUser user) {
		this.user = user;
	}
	
	

}
