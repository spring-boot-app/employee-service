package com.pv.employeeservice.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pv.employeeservice.dto.FICenterDTO;
import com.pv.employeeservice.model.FICenter;
import com.pv.employeeservice.repository.CenterRepository;
import com.pv.employeeservice.service.CenterService;

@Service
public class CenterServiceImpl implements CenterService{

	@Autowired
	private CenterRepository centerRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public FICenterDTO createCenter(FICenterDTO center) {
		FICenter fiCenter=modelMapper.map(center, FICenter.class);
		centerRepository.save(fiCenter);
		return modelMapper.map(fiCenter, FICenterDTO.class);
	}

	@Override
	public List<FICenterDTO> getAllCenter() {
		List<FICenter> centerList=centerRepository.findAll();
		if(centerList!=null && !centerList.isEmpty()) {
			return centerList.stream()
							.map(center->modelMapper.map(center, FICenterDTO.class))
							.collect(Collectors.toList());
		}
		return null;
	}

	@Override
	public FICenterDTO updateCenter(FICenterDTO center) {
		FICenter fiCenter=modelMapper.map(center, FICenter.class);
		//centerRepository.save(fiCenter,center.getId());
		return null;
	}
	
	
	
}
