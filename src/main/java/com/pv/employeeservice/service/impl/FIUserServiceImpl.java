package com.pv.employeeservice.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pv.employeeservice.dto.FIUserDTO;
import com.pv.employeeservice.model.FIUser;
import com.pv.employeeservice.repository.FIUserRepository;
import com.pv.employeeservice.service.FIUserService;

@Service
public class FIUserServiceImpl implements FIUserService {

	@Autowired
	private FIUserRepository userRepository;
	
	@Autowired
	ModelMapper modelMapper = new ModelMapper();
	
	@Override
	public void createUser(FIUserDTO userDTO) {
		FIUser user=modelMapper.map(userDTO,FIUser.class);
		userRepository.save(user);
		
	}

}
