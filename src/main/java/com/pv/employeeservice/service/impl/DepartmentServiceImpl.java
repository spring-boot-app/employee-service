package com.pv.employeeservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pv.employeeservice.dto.FIDepartmentDTO;
import com.pv.employeeservice.model.FIDepartment;
import com.pv.employeeservice.repository.DepartmentRepository;
import com.pv.employeeservice.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService{

	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public FIDepartmentDTO createDepartment(FIDepartmentDTO department) {
		FIDepartment departmentEntity=new FIDepartment();
		BeanUtils.copyProperties(department, departmentEntity);
		departmentRepository.save(departmentEntity);
		BeanUtils.copyProperties(departmentEntity,department);
		return department;
	}

	@Override
	public List<FIDepartmentDTO> getDepartments() {
		List<FIDepartment> departmentList=departmentRepository.findAll();
		List<FIDepartmentDTO> departments=new ArrayList<>();
		if(departmentList!=null && !departmentList.isEmpty()) {
			departments= departmentList
					  .stream()
					  .map(dept -> modelMapper.map(dept, FIDepartmentDTO.class))
					  .collect(Collectors.toList());
		}
		return departments;		
	}

}
