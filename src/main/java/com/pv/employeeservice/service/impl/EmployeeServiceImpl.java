package com.pv.employeeservice.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pv.employeeservice.dto.FIEmployeeDTO;
import com.pv.employeeservice.model.FIEmployee;
import com.pv.employeeservice.model.FIUser;
import com.pv.employeeservice.repository.EmployeeReposity;
import com.pv.employeeservice.repository.UserRepository;
import com.pv.employeeservice.service.EmployeeService;
@Service
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	private EmployeeReposity employeeRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	DateFormat formatter = new SimpleDateFormat("ddMMyyyy");
	@Override
	public void registerUser(FIEmployeeDTO employee) {
		FIEmployee fiEmployee=modelMapper.map(employee,FIEmployee.class);
		Integer userId=userRepository.max();
		userId=userId==null?userId=1:userId+1;
		FIUser user=fiEmployee.getUser();
		user.setUserName("PV-"+(userId<10?("0"+userId):userId));
		user.setPassword("PV"+formatter.format(employee.getDateOfBirth()));
		fiEmployee.setUser(user);
		employeeRepository.save(fiEmployee);
	}

	@Override
	public List<FIEmployee> fetchAllEmployee() {
		return employeeRepository.findAll();
	} 
}
