package com.pv.employeeservice.service;

import java.util.List;

import com.pv.employeeservice.dto.FIDepartmentDTO;

public interface DepartmentService {

	FIDepartmentDTO createDepartment(FIDepartmentDTO department);

	List<FIDepartmentDTO> getDepartments();
	
	

}
