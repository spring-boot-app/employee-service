package com.pv.employeeservice.service;

import java.util.List;

import com.pv.employeeservice.dto.FICenterDTO;

public interface CenterService {

	FICenterDTO createCenter(FICenterDTO center);
	
	List<FICenterDTO> getAllCenter();

	FICenterDTO updateCenter(FICenterDTO center);

}
