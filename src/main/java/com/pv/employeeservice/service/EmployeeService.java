package com.pv.employeeservice.service;

import java.util.List;

import com.pv.employeeservice.dto.FIEmployeeDTO;
import com.pv.employeeservice.model.FIEmployee;

public interface EmployeeService {

	void registerUser(FIEmployeeDTO employee);

	List<FIEmployee> fetchAllEmployee();

}
