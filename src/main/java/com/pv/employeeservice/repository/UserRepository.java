package com.pv.employeeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pv.employeeservice.model.FIUser;

@Repository
public interface UserRepository extends JpaRepository<FIUser, Integer> {

	@Query(value="select max(id) from FIUser")
	public Integer max();
}
