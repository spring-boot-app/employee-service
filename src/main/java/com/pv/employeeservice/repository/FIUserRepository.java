package com.pv.employeeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pv.employeeservice.model.FIUser;

@Repository
public interface FIUserRepository extends JpaRepository<FIUser, Integer> {
	
}
