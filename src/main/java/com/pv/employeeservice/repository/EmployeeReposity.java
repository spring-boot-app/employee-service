package com.pv.employeeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pv.employeeservice.model.FIEmployee;

@Repository
public interface EmployeeReposity extends JpaRepository<FIEmployee,Integer> {

}
